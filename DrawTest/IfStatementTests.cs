﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DrawingProgrammingLanguage
{
    [TestClass]
    public class IfStatementTests
    {
        [TestMethod]
        public void TestIfStatement_Equal()
        {
            // Arrange
            int var = 0;
            string mlthan = "==";
            int x = 5;
            string[] whilecom = new string[] { "move 10", "turn 90" };
            int[] varvalues = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            int x = 0;
            int angle = 0;
            IfStatement ifStatement = new IfStatement(var, mlthan, x, whilecom, varvalues, x, angle);

            // Act
            varvalues[var] = 5;
            ifStatement.If();

            // Assert
            Assert.AreEqual(10, x);
            Assert.AreEqual(90, angle);
        }

        [TestMethod]
        public void TestIfStatement_NotEqual()
        {
            // Arrange
            int var = 0;
            string mlthan = "==";
            int x = 5;
            string[] whilecom = new string[] { "move 10", "turn 90" };
            int[] varvalues = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            int x = 0;
            int angle = 0;
            IfStatement ifStatement = new IfStatement(var, mlthan, x, whilecom, varvalues, x, angle);

            // Act
            varvalues[var] = 3;
            ifStatement.If();

            // Assert
            Assert.AreEqual(0, x);
            Assert.AreEqual(0, angle);
        }
    }
}
