using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingProgrammingLanguage;
namespace DrawTest
{
    [TestClass]
    public class DrawTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            // arrange 
            
            int testx = 1;
            int testy = 1;
            int expectedx = 1;
            int expectedy = 1;
            
            //act
          myCanvess.moveto(testx, testy);
            
            //assert
            int actualx = Canvess.xPos;
            int actualy = Canvess.yPos;

            Assert.AreEqual(expectedy, actualy);
            Assert.AreEqual(expectedx, actualx);

        }
    }
}
