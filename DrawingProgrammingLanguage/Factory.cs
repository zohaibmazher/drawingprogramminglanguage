﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DrawingProgrammingLanguage
{
    /// <summary>
    /// Factory class for creating shape objects
    /// </summary>
    abstract class Factory
    {
        Graphics g;

        
    /// <summary>
    /// x and y coordinates for the shape
    /// </summary>
    protected int x, y;
        /// <summary>
        /// fill variable for the shape
        /// </summary>
        protected bool fill;

        /// <summary>
        /// constructor for the factory class
        /// </summary>
        /// <param name="fill">fill variable for the shape</param>
        /// <param name="x">x coordinate for the shape</param>
        /// <param name="y">y coordinate for the shape</param>
        public Factory(bool fill, int x, int y)
        {
            this.x = x;
            this.y = y;
            this.fill = fill;
        }

        /// <summary>
        /// abstract draw method for the factory class
        /// </summary>
        /// <param name="g">graphics object for drawing the shape</param>
        public abstract void draw(Graphics g);

    }
}