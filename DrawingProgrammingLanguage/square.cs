﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DrawingProgrammingLanguage
{
    /// <summary>
    /// Class for creating a square shape.
    /// </summary>
    class Square : Factory
    {
        /// <summary>
        /// Width of the square.
        /// </summary>
        int width;

        /// <summary>
        /// Constructor for initializing the square with fill, x, y and width.
        /// </summary>
        /// <param name="fill">Boolean value indicating if the shape should be filled or not.</param>
        /// <param name="x">The x-coordinate of the top-left corner of the shape.</param>
        /// <param name="y">The y-coordinate of the top-left corner of the shape.</param>
        /// <param name="width">The width of the square.</param>
        public Square(bool fill, int x, int y, int width) : base(fill, x, y)
        {
            this.width = width;
        }

        /// <summary>
        /// Method for drawing the square on the canvas.
        /// </summary>
        /// <param name="g">The Graphics object to use for drawing.</param>
        public override void draw(Graphics g)
        {
            // Checking if the square needs to be filled
            if (fill == true)
            {
                // Using the FillRectangle method of the Graphics class to fill the square on the canvas
                g.FillRectangle(Canvess.sb, x, y, x + width, y + width);
            }
            else
            {
                // Using the DrawRectangle method of the Graphics class to draw the square on the canvas
                g.DrawRectangle(Canvess.Pen, x, y, x + width, y + width);
            }
        }
    }
}
