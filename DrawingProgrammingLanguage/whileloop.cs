﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgrammingLanguage
{
    /// <summary>
    /// This class is responsible for implementing the while loop in the program.
    /// </summary>
    class Whileloop
    {
        //declaring variables
        int var;
        string mlthan;
        int x;
        string[] whilecom = new string[10];
        Form1 f = new Form1();

        
    /// <summary>
    /// Constructor for the Whileloop class
    /// </summary>
    /// <param name="var">The variable to be checked in the while loop</param>
    /// <param name="mlthan">The comparison operator to be used in the while loop</param>
    /// <param name="x">The value to compare the variable to</param>
    /// <param name="whilecom">The commands to be executed in the while loop</param>
    /// <param name="f">The Form1 object</param>
    public Whileloop(int var, string mlthan, int x, string[] whilecom, Form1 f)
        {
            this.var = var;
            this.mlthan = mlthan;
            this.x = x;
            this.whilecom = whilecom;
            this.f = f;

        }
        /// <summary>
        /// This method is responsible for executing the while loop
        /// </summary>
        public void Loop()
        {
            //check the value of mlthan
            switch (mlthan)
            {
                //if greater than
                case ">":
                    //check if var value is greater than x
                    while (Form1.varvalues[var] > x)
                    {
                        //loop through the commands
                        for (int i = 0; i < whilecom.Length; i++)
                        {
                            //check if the command is empty
                            if (string.IsNullOrEmpty(whilecom[i]))
                            {
                                break;
                            }
                            //execute the command
                            f.Task(whilecom[i]);
                            f.errorline++;

                        }
                    }
                    break;
                //if less than
                case "<":
                    //check if var value is less than x
                    while (Form1.varvalues[var] < x)
                    {

                        for (int i = 0; i < whilecom.Length; i++)
                        {
                            //check if the command is empty
                            if (string.IsNullOrEmpty(whilecom[i]))
                            {

                                break;

                            }
                            //execute the command
                            f.Task(whilecom[i]);
                            // line counter for error check 
                            f.errorline++;



                        }

                    }
                    break;

            }


        }
    }
}