﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DrawingProgrammingLanguage
{
    /// <summary>
    /// The class brushpos is used to create the brush position for the canvas.
    /// </summary>
    public class brushpos
    {
        // Declaring the Graphics object
        Graphics p;

        
            // Declaring the Pen object
            Pen Pen;
    int x;

        // Declaring the SolidBrush object
        SolidBrush sb = new SolidBrush(Color.Blue);

        /// <summary>
        /// Constructor for the brush position.
        /// </summary>
        /// <param name="p">The Graphics object that the brush position is being drawn on.</param>
        public brushpos(Graphics p)
        {
            this.p = p;

            // Creating a new Ellipse with the SolidBrush object
            p.FillEllipse(sb, 5 - 5, 5 - 5, 5 + 5, 5 + 5);

            // Creating a new Pen object with color Red and width 1
            Pen = new Pen(Color.Red, 1);
        }

        /// <summary>
        /// Method to move the brush position.
        /// </summary>
        public void point()
        {
            // Clearing the Graphics object
            p.Clear(Color.Transparent);
            // Creating a new Ellipse with the SolidBrush object at the current brush position
            p.FillEllipse(sb, Canvess.xPos - 5, Canvess.yPos - 5, 5 + 5, 5 + 5);
        }
    }
}