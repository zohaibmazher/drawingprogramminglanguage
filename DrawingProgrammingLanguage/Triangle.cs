﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DrawingProgrammingLanguage
{
    class Triangle: Factory
{
int sides;
    /// <summary>
    /// Initializes a new instance of the <see cref="Triangle"/> class.
    /// </summary>
    /// <param name="fill">if set to <c>true</c> the triangle will be filled.</param>
    /// <param name="x">The x coordinate of the top left corner of the triangle.</param>
    /// <param name="y">The y coordinate of the top left corner of the triangle.</param>
    /// <param name="side">The length of the sides of the triangle.</param>
    public Triangle(bool fill, int x, int y, int side) : base(fill, x, y)
    {
        this.sides = side;

        
        }

    /// <summary>
    /// Draws a triangle
    /// </summary>
    /// <param name="g">The graphics object to draw on</param>
    public override void draw(Graphics g)
    {
        //Define points of the triangle
        Point[] polygonPoints = new Point[3];
        int s2 = sides + sides;
        polygonPoints[0] = new Point(0, sides);
        polygonPoints[1] = new Point(sides, 10);
        polygonPoints[2] = new Point(s2, sides);

        //Check if fill is true or false
        if (fill == true)
        { g.FillPolygon(Canvess.sb, polygonPoints); }

        else
        { g.DrawPolygon(Canvess.Pen, polygonPoints); }

    }
}
}