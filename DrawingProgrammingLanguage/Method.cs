﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgrammingLanguage
{
    class Method
    {

        string[] values = new string[10];

        string[] methodcom = new string[10];
        string[] parameter;
        Form1 f = new Form1();



        public Method(string[] parameter,string[] values, string[] methodcom, Form1 f)
        {
            this.parameter = parameter;
            this.values = values;
            this.methodcom = methodcom;
            this.f = f;

        }


        public void Run()
        {
            for (int i = 0; i < methodcom.Length; i++)
            {
                //check if the command is empty
                if (string.IsNullOrEmpty(methodcom[i]))
                {

                    break;

                }
                if (methodcom[i] == "endmethod")
                {

                    break;

                }
                for (int i2 = 0; i2 < parameter.Length; i2++)
                {
                    if (methodcom[i].Contains(parameter[i2])){
                        string[] subs = methodcom[i].Split(' ');
                       
                        
                        f.Task(subs[0]+" "+values[i2]);
                        f.errorline++;
                        break;
                    }
                    else
                    {
                        //execute the command
                        f.Task(methodcom[i]);
                        // line counter for error check 
                        f.errorline++;
                    }
                }
                   



            }
        }
    }
}