﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DrawingProgrammingLanguage
{
    /// <summary>
    /// This class creates a rectangle shape and provides methods for drawing the rectangle on a canvas
    /// </summary>
    class Rectangle : Factory
    {
        int width, height;

        /// <summary>
        /// Constructor for initializing the rectangle with fill, x, y, width, and height
        /// </summary>
        /// <param name="fill">Boolean value indicating if the rectangle should be filled or not</param>
        /// <param name="x">X coordinate of the rectangle</param>
        /// <param name="y">Y coordinate of the rectangle</param>
        /// <param name="widith">Width of the rectangle</param>
        /// <param name="height">Height of the rectangle</param>
        public Rectangle(bool fill, int x, int y, int widith, int height) : base(fill, x, y)

        {
            this.width = widith;

            this.height = height;
        }

        /// <summary>
        /// Method for drawing the rectangle on the canvas
        /// </summary>
        /// <param name="g">Graphics object on which the rectangle is to be drawn</param>
        public override void draw(Graphics g)
        {
            // Checking if the square needs to be filled
            if (fill == true)
            // Using the FillRectangle method of the Graphics class to fill the square on the canvas
            { g.FillRectangle(Canvess.sb, x, y, x + height, y + width); }
            // Using the DrawRectangle method of the Graphics class to draw the square on the canvas
            else
            { g.DrawRectangle(Canvess.Pen, x, y, x + height, y + width); }
        }
    }
}