﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace DrawingProgrammingLanguage
{
    /// <summary>
    /// Class for creating a circle shape.
    /// </summary>
    class Circle : Factory
    {
        /// <summary>
        /// Radius of the circle.
        /// </summary>
        int radius;

        /// <summary>
        /// Constructor for initializing the circle with fill, x, y and radius.
        /// </summary>
        /// <param name="fill">Determines whether the circle should be filled or not.</param>
        /// <param name="x">The x-coordinate of the center of the circle.</param>
        /// <param name="y">The y-coordinate of the center of the circle.</param>
        /// <param name="radius">The radius of the circle.</param>
        public Circle(bool fill, int x, int y, int radius) : base(fill, x, y)
        {
            // Assigning the radius passed in the constructor to the class variable
            this.radius = radius;
        }

        /// <summary>
        /// Method for drawing the circle on the canvas.
        /// </summary>
        /// <param name="g">The Graphics object to draw the circle on.</param>
        public override void draw(Graphics g)
        {
            // Checking if the circle needs to be filled
            if (fill == true)
            {
                // Using the FillEllipse method of the Graphics class to fill the circle on the canvas
                g.FillEllipse(Canvess.sb, x - radius, y - radius, radius + radius, radius + radius);
            }
            else
            {
                // Using the DrawEllipse method of the Graphics class to draw the circle on the canvas
                g.DrawEllipse(Canvess.Pen, x - radius, y - radius, radius + radius, radius + radius);
            }
        }
    }
}
