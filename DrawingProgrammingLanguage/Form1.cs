﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace DrawingProgrammingLanguage
{
    public partial class Form1 : Form
    {

        public static int xPos;
        public static int yPos;
        Graphics g;
        public static Pen Pen;

        public static SolidBrush sb = new SolidBrush(Color.Black);

        // Bitmap OutputBitmap = new Bitmap(511, 426);
        //  Bitmap Dot = new Bitmap(511,426);
        //  Canvess mycanvass;

        // bool solid = false;    
        //  public Form1()
        //  {
        //InitializeComponent();

        //  mycanvass = new Canvess(Graphics.FromImage(OutputBitmap));
        // mycanvass = new Dot(OutputBitmap);

        public static int screenx = 511;
        public static int screeny = 426;
        Bitmap OutputBitmap = new Bitmap(screenx, screeny);
        Bitmap Dot = new Bitmap(screenx, screeny);
        Canvess mycanvass;
        Factory myfactory;
        brushpos mybrushpos;
        public static int[] varvalues = new int[10];
        public static string[] vnames = new string[10];
        int numvar = 1;
       string[] whilecom = new string[10];
        string[] methodcom = new string[10];
        Boolean whilecommand = false;
        String endloop = "endloop";
        int var = 0;
        string mlthan = "";
        int whlx; 
        bool solid = false;
        public int errorline = 1;
        Boolean methodcommand = false;
        string methodname;
        string[] parameter;
        public Form1()
        {

            InitializeComponent();

            mybrushpos = new brushpos(Graphics.FromImage(Dot));
            mycanvass = new Canvess(Graphics.FromImage(OutputBitmap));
            Graphics g = Graphics.FromImage(OutputBitmap);
            // myfactory= new Factory(Graphics.FromImage(OutputBitmap));





        }

        private void DrawCircle (int tempxc)
        {

            Graphics g = Graphics.FromImage(OutputBitmap);
            Circle circ = new Circle(solid, Canvess.xPos, Canvess.yPos, tempxc);
            circ.draw(g);





        }
        private void button1_Click(object sender, EventArgs e)
        {
            String Commands = CommandBox.Text.Trim().ToLower();
            string[] subs = Commands.Split('\n');
            int counter = 0;
            int counter1 = 0;
            foreach (var sub in subs)
            {
                if (sub == endloop)
                {
                    
                    whilecommand = false;
                    Console.WriteLine("loop end");
                }
                if (whilecommand == true)
                {
                 
                    whilecom[counter] = sub;
                    //Console.WriteLine(whilecom[counter]);
                    counter++;
                  //  errorline++;

                }
                if (methodcommand == true)
                {
                    methodcom[counter1] = sub;
                    counter1++;
                    whilecommand = true;
                }


               if (whilecommand == false)
                {
                    //   System.Console.WriteLine(sub);
                    Task(sub);
                    Refresh();
                    errorline++;


                }
                if (sub == "endmethod")
                {
                    methodcommand = false;
                    whilecommand = false;
                }
                if (sub == "end")
                {
                    IfStatement ifs = new IfStatement(var, mlthan, whlx, whilecom, this);
                    ifs.If();
                    whilecommand = false;
                }

            }
            Console.WriteLine("Run Button", subs);
            mybrushpos.point();
            errorline = 1;
            Refresh();
        }

        private void CommandBox_KeyDown(object sender, KeyEventArgs e)
        {


        }



        public void Task(string Command)
        {
            bool valid = false;
            

            bool matchFound1 = false;
            for (int i = 1; i < vnames.Length; i++)
            {
                try {
                    if (string.IsNullOrEmpty(Command) || string.IsNullOrEmpty(vnames[i]))
                        continue;

                    string[] subs = Command.Split(' ');


                    if (subs[0] == (vnames[i]))
                    {
                        if (subs[1] == "=")
                        {
                            if (subs[2] == (vnames[i]))
                            {
                                matchFound1 = true;
                                if (subs[3] == "+")
                                {
                                    int x = Int32.Parse(subs[4]);
                                    varvalues[i] = varvalues[i] + x;
                                    valid = true;
                                    Console.WriteLine("variable increased "+ subs[4]);
                                }
                                else if (subs[3] == "-")
                                {
                                    int x = Int32.Parse(subs[4]);
                                    varvalues[i] = varvalues[i] - x;
                                    valid = true;
                                }
                            }
                        }

                        break;
                    }
                }
                catch (Exception)
                {

                }
            }



            if (Command.Contains("while") == true)
            {


                valid = true;
                string[] subs = Command.Split(' ');

                try
                {
                    for (int i = 1; i < vnames.Length; i++)
                    {
                        if (subs[1] == (vnames[i]))
                        {
                            Console.WriteLine("variable found ");

                            var = i;


                            break;
                        }
                        else { valid = false; }
                    }




                    if (subs[2] == "<")
                    {
                        Console.WriteLine("Command <");
                        whilecommand = true;

                        mlthan = "<";
                        whlx = Int32.Parse(subs[3]);






                        Console.WriteLine("loop");


                    }
                    else if (subs[2] == ">")
                    {
                        whilecommand = true;
                        Console.WriteLine("Command >");
                        mlthan = ">";
                    }
                    else
                    {
                        valid = false;
                    }
                }

                catch (FormatException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                //  Console.WriteLine(subs[3]);







            }

            if (Command.Contains("endloop") == true)
            {
                //  Whileloop whloop = new Whileloop(var, mlthan, whlx, whilecom);
                //  whloop.Loop();
                Whileloop wl = new Whileloop(var, mlthan, whlx, whilecom, this);
                wl.Loop();
                
                valid = true;
            }

            if (Command.Contains("if") == true)
            {


                valid = true;
                string[] subs = Command.Split(' ');

                try
                {
                    for (int i = 1; i < vnames.Length; i++)
                    {
                        if (subs[1] == (vnames[i]))
                        {
                            Console.WriteLine("variable found ");

                            var = i;


                            break;
                        }
                        else { valid = false; }
                    }

                    if (subs[2] == "==")
                    {
                        Console.WriteLine("Command ==");
                        whilecommand = true;

                        mlthan = "==";
                        whlx = Int32.Parse(subs[3]);

                        Console.WriteLine("if");


                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }






            }

            if (Command.Contains("end") == true)
            {
                whilecommand = false;
                valid = true;
            }




            if (Command.Contains("drawto") == true)
            {
                valid = true;
                string[] subs = Command.Split(' ');
                try
                {
                    int xc = 0;
                    string x = subs[1];
                    bool matchFound = false;

                    for (int i = 0; i < vnames.Length; i++)
                    {
                        if (vnames[i] == x)
                        {
                            matchFound = true;
                            xc = varvalues[i];
                            break;
                        }
                    }
                    if (matchFound)
                    {
                        Console.WriteLine("Match found.");
                    }
                    else
                    {
                        Console.WriteLine("Match not found.");
                        xc = int.Parse(x);
                    }
                    int xc2 = 0;
                    string x2 = subs[2];
                    matchFound = false;

                    for (int i = 0; i < vnames.Length; i++)
                    {
                        if (vnames[i] == x2)
                        {
                            matchFound = true;
                            xc2 = varvalues[i];
                            break;
                        }
                    }
                    if (matchFound)
                    {
                        Console.WriteLine("Match found.");
                    }
                    else
                    {
                        Console.WriteLine("Match not found.");
                        xc2 = int.Parse(x2);
                    }
                    mycanvass.DrawLine(xc, xc2);
                }
                catch (FormatException)
                {
                    MessageBox.Show("parameters invalid.");
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("parameters invalid.");
                }
                Console.WriteLine("line");

            }

            else if (Command.Contains("new variable"))
            {
                valid = true;
                string[] subs = Command.Split(' ');
                try
                {

                    string x = subs[2];
                    string x2 = (subs[3]);
                    int x3 = Int32.Parse(subs[4]);
                    vnames[numvar] = x;
                    varvalues[numvar] = x3;

                    if (x2 != "=")
                    {

                        throw new IndexOutOfRangeException();


                    }
                    else { numvar++; }
                    Console.WriteLine(x);
                    Console.WriteLine(x2);
                    Console.WriteLine(x3);
                    // mycanvass.DrawTriangle(x, solid);
                    //Triangle trian = new Triangle(solid, Canvess.xPos, Canvess.yPos, x);
                    //trian.draw(g);

                }
                catch (FormatException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                Console.WriteLine("new variable");
            }


          ///  else if (Command.Contains("print") == true)
         ///   {
           ///     for (int i = 0; i < whilecom.Length; i++)
           ///     {
           ///        Console.WriteLine(whilecom[i]);
              ///  }
            ///}
           


            else if (Command.Contains("square") == true)
            {
                valid = true;
                string[] subs = Command.Split(' ');
                try
                {

                    int xc = 0;
                    string x = subs[1];
                    bool matchFound = false;

                    for (int i = 0; i < vnames.Length; i++)
                    {
                        if (vnames[i] == x)
                        {
                            matchFound = true;
                            xc = varvalues[i];
                            break;
                        }
                    }
                    if (matchFound)
                    {
                        Console.WriteLine("Match found.");
                    }
                    else
                    {
                        Console.WriteLine("Match not found.");
                        xc = int.Parse(x);
                    }


                    Graphics g = Graphics.FromImage(OutputBitmap);


                    //mycanvass.DrawSquare(x, solid);
                    Square squar = new Square(solid, Canvess.xPos, Canvess.yPos, xc);
                    squar.draw(g);
                  
                }
                catch (FormatException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }

                Console.WriteLine("square");

            }




            else if (Command.Contains("rect"))
            {
                valid = true;
                string[] subs = Command.Split(' ');
                try
                {
                    int xc = 0;
                    string x = subs[1];
                    bool matchFound = false;

                    for (int i = 0; i < vnames.Length; i++)
                    {
                        if (vnames[i] == x)
                        {
                            matchFound = true;
                            xc = varvalues[i];
                            break;
                        }
                    }
                    if (matchFound)
                    {
                        Console.WriteLine("Match found.");
                    }
                    else
                    {
                        Console.WriteLine("Match not found.");
                        xc = int.Parse(x);
                    }
                    int xc2 = 0;
                    string x2 = subs[2];
                    matchFound = false;

                    for (int i = 0; i < vnames.Length; i++)
                    {
                        if (vnames[i] == x2)
                        {
                            matchFound = true;
                            xc2 = varvalues[i];
                            break;
                        }
                    }
                    if (matchFound)
                    {
                        Console.WriteLine("Match found.");
                    }
                    else
                    {
                        Console.WriteLine("Match not found.");
                        xc2 = int.Parse(x2);
                    }

                    Graphics g = Graphics.FromImage(OutputBitmap);
                    Rectangle rec = new Rectangle(solid, Canvess.xPos, Canvess.yPos, xc, xc2);
                    rec.draw(g);

                }
                catch (FormatException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                Console.WriteLine("rectangle");

            }


            
            else if (Command.Contains("circle"))
            {
                valid = true;
                string[] subs = Command.Split(' ');
                try
                {
                    int xc = 0;
                    string x = subs[1];
                    bool matchFound = false;

                    for (int i = 0; i < vnames.Length; i++)
                    {
                        if (vnames[i] == x)
                        {
                            matchFound = true;
                            xc = varvalues[i];
                            break;
                        }
                    }
                    if (matchFound)
                    {
                        Console.WriteLine("Match found.");
                    }
                    else
                    {
                        Console.WriteLine("Match not found.");
                        xc = int.Parse(x);
                    }
                    //  Graphics g = Graphics.FromImage(OutputBitmap);
                    //  Circle circ = new Circle(solid, Canvess.xPos, Canvess.yPos, xc);
                    //  circ.draw(g);

                    DrawCircle(xc);

                
                }
                catch (FormatException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                Console.WriteLine("circle");

            }

           else if (Command.Contains("method"))
            {
                valid = true;
                string[] subs = Command.Split(' ');
               
                methodname = subs[1];
                
                Console.WriteLine(methodname);
                Match match = Regex.Match(Command, @"\(([^)]*)\)");
                parameter = match.Groups[1].Value.Split(' ');
                Console.WriteLine(string.Join(",", parameter));
                methodcommand = true;
            }

            else if (Command.Equals("endmethod"))
            {
                valid = true;
                whilecommand = false;
            }

            else if (Command.Contains(methodname))
            {
                valid = true;
                Console.WriteLine(methodname + " method found");
                Match match = Regex.Match(Command, @"\(([^)]*)\)");
                string[] values = match.Groups[1].Value.Split(' ');
                Console.WriteLine(string.Join(",", values));
                Method med = new Method(parameter,values, methodcom, this);
                med.Run();
            }



            else if (Command.Contains("triangle"))
            {
                valid = true;
                string[] subs = Command.Split(' ');
                try
                {

                    int xc = 0;
                    string x = subs[1];
                    bool matchFound = false;

                    for (int i = 0; i < vnames.Length; i++)
                    {
                        if (vnames[i] == x)
                        {
                            matchFound = true;
                            xc = varvalues[i];
                            break;
                        }
                    }
                    if (matchFound)
                    {
                        Console.WriteLine("Match found.");
                    }
                    else
                    {
                        Console.WriteLine("Match not found.");
                        xc = int.Parse(x);
                    }


                    Graphics g = Graphics.FromImage(OutputBitmap);
                    Triangle trian = new Triangle(solid, Canvess.xPos, Canvess.yPos, xc);
                    trian.draw(g);

                }
                catch (FormatException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("parameters invalid line " + errorline);
                }
                Console.WriteLine("triangle");
            }



            else if (Command.Equals("pen red"))
            {
                valid = true;
                mycanvass.penred();
                Console.WriteLine("pen red");

            }



            

            else if (Command.Equals("pen black"))
            {
                valid = true;
                mycanvass.penblack();
                Console.WriteLine("pen black");

            }

            else if (Command.Equals("pen green"))
            {
                valid = true;
                mycanvass.pengreen();
                Console.WriteLine("pen green");

            }

            else if (Command.Equals("pen yellow"))
            {
                valid = true;
                mycanvass.penyellow();
                Console.WriteLine("pen yellow");

            }
            else if (Command.Equals("reset"))
            {
                valid = true;
                mycanvass.reset();
            }

            else if (Command.Equals("clear"))
            {
                valid = true;
                mycanvass.clear();

            }

            else if (Command.Equals("fill on"))
            {
                valid = true;
                solid = true;

            }
            else if (Command.Equals("fill off"))
            {
                valid = true;
                solid = false;

            }

            if (Command.Contains("moveto") == true)
            {
                valid = true;
                string[] subs = Command.Split(' ');
                try
                {
                    int xc = 0;
                    string x = subs[1];
                    bool matchFound = false;

                    for (int i = 0; i < vnames.Length; i++)
                    {
                        if (vnames[i] == x)
                        {
                            matchFound = true;
                            xc = varvalues[i];
                            break;
                        }
                    }
                    if (matchFound)
                    {
                        Console.WriteLine("Match found.");
                    }
                    else
                    {
                        Console.WriteLine("Match not found.");
                        xc = int.Parse(x);
                    }
                    int xc2 = 0;
                    string x2 = subs[2];
                    matchFound = false;

                    for (int i = 0; i < vnames.Length; i++)
                    {
                        if (vnames[i] == x2)
                        {
                            matchFound = true;
                            xc2 = varvalues[i];
                            break;
                        }
                    }
                    if (matchFound)
                    {
                        Console.WriteLine("Match found.");
                    }
                    else
                    {
                        Console.WriteLine("Match not found.");
                        xc2 = int.Parse(x2);
                    }

                    mycanvass.pos(xc, xc2);
                }
                catch (FormatException)
                {
                    MessageBox.Show("parameters invalid.");
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("parameters invalid.");
                }
                Console.WriteLine("position");
            }
            if (Command.Equals("run"))
            {
                valid = false;
            }
            else if (valid == false)
            {
               
                MessageBox.Show("invalid command line "+ errorline);

            }


        }


        private void CommandLine_KeyDown(object sender, KeyEventArgs e)
        {


            if (e.KeyCode == Keys.Enter)
            {
                String Command = CommandLine.Text.Trim().ToLower();

                Task(Command);
                if (Command.Equals("run"))
                {

                    button1_Click(new object(), new EventArgs());

                }
                CommandLine.Text = "";
                mybrushpos.point();
                Refresh();

            }
        }
        private void canvas_Paint(object sender, PaintEventArgs e)
        {

            Graphics g = e.Graphics;
            g.DrawImageUnscaled(OutputBitmap, 0, 0);
            Graphics p = e.Graphics;
            p.DrawImageUnscaled(Dot, 0, 0);


            //Graphics g = e.Graphics;
            // g.DrawImageUnscaled(OutputBitmap, 0, 0);
            //comit test 
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (Stream s = File.Open(saveFileDialog1.FileName, FileMode.CreateNew))
                using (StreamWriter sw = new StreamWriter(s))
                {
                    sw.Write(CommandBox.Text);
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if ((myStream = openFileDialog1.OpenFile()) != null)
                {
                    string strfilename = openFileDialog1.FileName;
                    string filetext = File.ReadAllText(strfilename);
                    CommandBox.Text = filetext;


                }
            }

        }

        private void canvas_Click(object sender, EventArgs e)
        {

        }
    }

}
