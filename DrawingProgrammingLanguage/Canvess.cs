﻿using System.Drawing;
using System.Threading;

namespace DrawingProgrammingLanguage
{
    // Class for creating a canvas for drawing
    public class Canvess
    {
        // x and y position of the pen
        public static int xPos;
        public static int yPos;
        // Graphics object to perform drawing operations
        Graphics g;
        // Pen object for drawing lines
        public static Pen Pen;
        // SolidBrush object for filling shapes
        public static SolidBrush sb = new SolidBrush(Color.Black);


        /// <summary>
        /// Constructor for initializing the canvas
        /// </summary>
        /// <param name="g">Graphics object to perform drawing operations</param>
        public Canvess(Graphics g)
        {
            // Assigning the graphics object passed in the constructor to the class variable
            this.g = g;
            // Setting the initial x and y position of the pen to 0
            xPos = yPos = 0;
            // Initializing the pen with black color and width of 1
            Pen = new Pen(Color.Black, 1);
        }

        /// <summary>
        /// Method for setting the pen color to red
        /// </summary>
        public void penred()
        {
            Pen.Color = Color.Red;
            sb.Color = Color.Red;
        }

        /// <summary>
        /// Method for setting the pen color to black
        /// </summary>
        public void penblack()
        {
            Pen.Color = Color.Black;
            sb.Color = Color.Black;
        }

        /// <summary>
        /// Method for setting the pen color to green
        /// </summary>
        public void pengreen()
        {
            Pen.Color = Color.Green;
            sb.Color = Color.Green;
        }

        /// <summary>
        /// Method for setting the pen color to yellow
        /// </summary>
        public void penyellow()
        {
            Pen.Color = Color.Yellow;
            sb.Color = Color.Yellow;
        }

        /// <summary>
        /// Method for drawing a line from the current position of the pen to the specified x and y position
        /// </summary>
        /// <param name="toX">End point x-coordinate of the line</param>
        /// <param name="toY">End point y-coordinate of the line</param>
        public void DrawLine(int toX, int toY)
        {
            // Using the DrawLine method of the Graphics class to draw a line from the current position of the pen to the specified x and y position
            g.DrawLine(Pen, xPos, yPos, toX, toY);
            // Updating the current position of the pen to the end point of the line
            xPos = toX;
            yPos = toY;
        }

        /// <summary>
        /// Method for resetting the pen to its initial position and color
        /// </summary>
        public void reset()
        {
            xPos = yPos = 0;
            Pen.Color = Color.Black;
        }

        /// <summary>
        /// Method for clearing the canvas
        /// </summary>
        public void clear()
        {
            xPos = yPos = 0;
            g.Clear(Color.Transparent);
        }
        /// <summary>
        /// Methord for movving position on canvas
        /// </summary>
        public void pos(int x, int y)
        {
            xPos = x;
            yPos = y;

        }

     
    }
}
