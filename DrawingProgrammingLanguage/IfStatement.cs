﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgrammingLanguage
{
    /// <summary>
    /// Represents an if statement in the program
    /// </summary>
    class IfStatement
    {
        int var;
        string mlthan;
        int x;
        string[] whilecom = new string[10];
        Form1 f = new Form1();

        /// <summary>
        /// Initializes a new instance of the <see cref="IfStatement"/> class.
        /// </summary>
        /// <param name="var">The variable to be used in the if statement.</param>
        /// <param name="mlthan">The comparison operator.</param>
        /// <param name="x">The value to compare the variable to.</param>
        /// <param name="whilecom">The commands to be executed if the statement is true.</param>
        /// <param name="f">The main form of the program.</param>
        public IfStatement(int var, string mlthan, int x, string[] whilecom, Form1 f)
        {
            this.var = var;
            this.mlthan = mlthan;
            this.x = x;
            this.whilecom = whilecom;
            this.f = f;
        }

        /// <summary>
        /// Executes the if statement.
        /// </summary>
        public void If()
        {
            //check the value of mlthan
            switch (mlthan)
            {
                case "==":
                    //check if var value is equal to x
                    if (Form1.varvalues[var] == x)
                    {
                        //loop through the commands
                        for (int i = 0; i < whilecom.Length; i++)
                        {
                            //check if the command is empty
                            if (string.IsNullOrEmpty(whilecom[i]))
                            {
                                break;
                            }
                            //execute the command
                            f.Task(whilecom[i]);
                            f.Refresh();
                            //increment the error line
                            f.errorline++;
                        }
                    }
                    break;
            }
        }
    }
}