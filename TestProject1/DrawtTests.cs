using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingProgrammingLanguage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace TestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string inputed_command = "circle 20";
            int expected = 20;
            Form1 Task = new Task(string inputed_command);

            int actual = Task.x;

            Assert.AreEqual(expected,actual, "split not working correctly " );

        }
    }
}
